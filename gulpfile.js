var gulp = require('gulp'),
    browsersync = require('browser-sync').create(), // для перезагрузки браузера
    concat 		= require('gulp-concat'), // для соединения файлов в один - конкатенация
    minify 		= require('gulp-minify'), // для сжатия js
    cssnano 	= require('gulp-cssnano'), // для сжатия css
    rename 		= require('gulp-rename'), // для переименования
    del 		= require('del'), // Delete files and folders using globs
    autoprefixer= require('gulp-autoprefixer'), // для авто-расстановки префиксов в css
    htmlmin 	= require('gulp-htmlmin'), // минимайзер для html
    pug         = require('gulp-pug'), // препроцессор для html
    notify      = require("gulp-notify"),
    cleancss    = require("gulp-clean-css"),
    sass 		= require('gulp-sass'); // для работы с sass, препроцессор для css

// src - пути к файлам проекта, dest - папка в которую gulp пернесет файлы
var paths = {
    styles: {
        src: 'app/styles/sass/style.sass',
        dest: 'dist/css/'
    },
    templates: {
        src: 'app/templates/pug/pages/*.pug',
        dest: 'dist/'
    },
    navigationFile: {
        src: 'app/templates/pug/pages/index.pug',
        dest: './'
    },
    cssfiles:{
        src: 'app/libs/css/**/*.css',
        dest: 'dist/css'
    },
    // jquery must be first
    jquery:{
        src: 'app/libs/js/jquery.js',
        dest: 'dist/js'
    },
    jsfiles:{
        src: 'app/libs/js/**/*.js',
        dest: 'dist/js'
    },
    img:{
        src: 'app/img/**/*',
        dest: 'dist/img'
    },
    fonts:{
        src: 'app/fonts/**/*',
        dest: 'dist/fonts'
    },
};

function clean() {
    return del([
        'dist/*'
        //,'index.html'
    ]);
}

function img() {
    return gulp.src(paths.img.src)
        .pipe(gulp.dest(paths.img.dest))
        .pipe(browsersync.stream());
}

function libscss() {
    return gulp.src(paths.cssfiles.src)
        .pipe(concat('libs.min.css'))
        .pipe(cssnano())
        .pipe(gulp.dest(paths.cssfiles.dest))
        .pipe(browsersync.stream());
}

function libsjs() {
    return gulp.src(paths.jsfiles.src)
        .pipe(concat('libs.js'))
        .pipe(minify())
        .pipe(gulp.dest(paths.jsfiles.dest))
        .pipe(browsersync.stream());
}

function fonts() {
    return gulp.src(paths.fonts.src)
        .pipe(gulp.dest(paths.fonts.dest))
        .pipe(browsersync.stream());
}

function jquery() {
    return gulp.src(paths.jquery.src)
        .pipe(concat('jquery.js'))
        .pipe(minify())
        .pipe(gulp.dest(paths.jquery.dest))
        .pipe(browsersync.stream());
}

function styles() {
    return gulp.src(paths.styles.src)
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(cssnano())
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(browsersync.stream());
}

function templates() {
    return gulp.src(paths.templates.src)
        .pipe(pug())
        .pipe(gulp.dest(paths.templates.dest))
        .pipe(browsersync.stream());
}

function navigation() {
    return gulp.src(paths.navigationFile.src)
        .pipe(pug())
        .pipe(gulp.dest(paths.navigationFile.dest))
        .pipe(browsersync.stream());
}

function watchFiles() {
    browsersync.init({
        server: {
            //proxy: "your.dev"
            baseDir: "./"
        },
        /*
        port: 8080,
        open: true,
        notify: false,
        // позволит ссылаться на проект с других устройств через локальный адрес, адрес появится в консоли
        tunnel: true
        */
    });

    gulp.watch("app/styles/sass/**/*.sass", styles);
    gulp.watch("app/templates/pug/**/*.pug", templates);
    gulp.watch("app/templates/pug/pages/index.pug", navigation);
    gulp.watch("app/libs/js/jquery.js", jquery);
    gulp.watch("app/libs/css/*.css", libscss);
    gulp.watch("app/libs/js/*.js", libsjs);
    gulp.watch("app/img/**/*", img);
    gulp.watch("app/fonts/**/*", fonts);
}

gulp.task('clean', clean);
gulp.task('watch', watchFiles);

// gulp.series - последовательное выполнение задач
// gulp.parallel - параллельное выполнение задач
gulp.task('build', gulp.series(clean,gulp.parallel(styles,templates,jquery,libsjs,libscss,navigation,img,fonts)));

// запускаем проект командой - gulp start
gulp.task('start',gulp.series('build','watch'));