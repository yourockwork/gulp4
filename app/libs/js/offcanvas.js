$(document).ready(function () {
    $('.btn-side--js').click(function () {
      $(this).toggleClass('active');
      $('.row-offcanvas').toggleClass('active');
    });
    $('.navbar-collapse')
        .on('shown.bs.collapse', function () {
            $('.navbar-toggle--js').addClass('active')
    })
        .on('hidden.bs.collapse', function () {
            $('.navbar-toggle--js').removeClass('active')
    });
});